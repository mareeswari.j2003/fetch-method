//Response handled by using then method
fetch('https://fakestoreapi.com/products')

.then((resp) =>{
    console.log(resp)
})
.catch((error)=>{
    console.log(error);
})





























//Error handled by using catch method
// fetch('https://fakestoreapi1234.com/products')

// .then((resp) =>{
//     console.log(resp)
// })
// .catch((error)=>{
//     console.log(error);
// })
